package com.epamaqa;

import java.util.Scanner;
import java.lang.*;



/**
 * Created by Max on 023, 23.07.2019.
 */
public class Party {
    public static void main(String[] args) {
        final int attemptsAmount = 50;//amount of attempts
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter the number of people at the party(more then 2!): ");
        int numOfParticipants = scan.nextInt();
        int countTimesAllSpreaded = 0;//how many times all participiants heard the ruor
        int peopleHeard = 0;//amount of people with all attempts

        for (int i = 0; i < attemptsAmount; i++) {
            boolean participants[] = new boolean[attemptsAmount]; // Alice 0 element, Bob is 1, other  quests n+
            participants[1] = true; //Bob already heard
            boolean alreadyHeard = false; //to exit while
            int nextPerson = -1; //our random next person
            int currentPerson = 1; //start from Bob

            while (!alreadyHeard) {
                nextPerson = 1 + (int) (Math.random() * (attemptsAmount - 1)); //randomize next person
                if (nextPerson == currentPerson) { // check that it's not our current person
                    while (nextPerson == currentPerson) //if it's true we get other person
                        nextPerson = 1 + (int) (Math.random() * (attemptsAmount - 1));
                }

                if(participants[nextPerson]) //if guest already heard
                {
                if(rumorSpreaded(participants)) //if all people heard
                    countTimesAllSpreaded++;
                    peopleHeard = peopleHeard + countPeopleReached(participants); //how many people we get at all
                    alreadyHeard = true; // our condition is true
                }
            participants[nextPerson] = true; //now nextperson heard rumor
            currentPerson = nextPerson; //current person now is nextperson
            }
        }
    //For bigger N we will have less empirical probability
	System.out.println("Probability that everyone will hear rumor except Alice in "+attemptsAmount+" attempts: " +
            (double)countTimesAllSpreaded/attemptsAmount);
	System.out.println("Average amount of quests that rumor reached is: "+peopleHeard/attemptsAmount);
        }
        public static int countPeopleReached(boolean arr[]){
            int counter = 0;
            for(int i = 1;i<arr.length;i++)
                if(arr[i])
                counter++;
                return counter;
        }

        public static boolean rumorSpreaded(boolean arr[]){
            for(int i = 1;i<arr.length;i++)
            if(!arr[i])
                return false;
            return true;

        }
}